package com.kometsales.infoProducts.controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.kometsales.infoProducts.mapper.ProductMapper;
import com.kometsales.infoProducts.model.Product;
import com.kometsales.infoProducts.repository.ProductRepository;


@RestController
@RequestMapping("/api")
public class ProductController {
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ProductMapper productMapper;
	
	@GetMapping(path = "/products")
	public Set<Product> getAllVisit(){
		Set<Product> visits = new HashSet<Product>();
		productRepository.findAll().forEach(visit->visits.add(visit));
		return productMapper.mapper(visits);
	}
	
	@GetMapping(path = "/product/{id}")
	public Product getVisitById(@PathVariable("id") Integer id) {
		Product product = productRepository.findById(id).get();
		return productMapper.mapper(product);
	}

	@PostMapping(path = "/product")
	public String createProduct(@RequestBody Product product) {
		productRepository.save(product);
		return "Visit Created!";
	}
	
	@PutMapping(path = "/product/{id}")
	public String updateVisit(@PathVariable("id") int id, @RequestBody Product product) {
		productRepository.save(product);
		return "product Update!";
	}
	
	@DeleteMapping(path = "/product/{id}")
	public String delete(@PathVariable("id") int id) {
		productRepository.deleteById(id);
		return "Visit Deleted";
	}

}
