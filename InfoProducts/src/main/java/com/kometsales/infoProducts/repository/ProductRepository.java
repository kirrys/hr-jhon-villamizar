package com.kometsales.infoProducts.repository;

import org.springframework.data.repository.CrudRepository;

import com.kometsales.infoProducts.model.Product;

public interface ProductRepository extends CrudRepository<Product, Integer>{

}
