package com.kometsales.infoProducts.mapper;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kometsales.infoProducts.model.Product;

@Service("ProductMapper")
public class ProductMapper {
		
	@Autowired
	ProductMapper productMapper;
		
		public Set<Product> mapper(Set<Product> product){
			Set<Product> products = new HashSet<Product>();
			for(Product pr : product) {
				Product pr2= mapper(pr);
				products.add(pr2);
			}
			return products;
		}
		
		public Product mapper(Product pr) {
			Product pr2= new Product();
			pr2.setId(pr.getId());
			pr2.setRef(pr.getRef());
			pr2.setName(pr.getName());
			pr2.setQuantity(pr.getQuantity());
			pr2.setPromotion(pr.getPromotion());
			pr2.setCreatedAt(pr.getCreatedAt());
			pr2.setUpdatedAt(pr.getUpdatedAt());
			
			return pr2;
		}

}
