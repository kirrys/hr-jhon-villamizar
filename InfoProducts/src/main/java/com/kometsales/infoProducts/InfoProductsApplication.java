package com.kometsales.infoProducts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfoProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfoProductsApplication.class, args);
	}

}
